SCREEN_SIZE = { 12, 4 }

gpu = computer.getPCIDevices(classes["GPU_T1_C"])[1]

screen = component.proxy(component.findComponent(classes["Build_screen_C"]))[1]
-- screen = computer.getPCIDevices(classes["FINComputerScreen"])[1]
-- screen = component.proxy( component.findComponent(classes["Screen"])[1])

function initScreen()
  gpu:bindScreen(screen)
  gpu:setSize(SCREEN_SIZE[1], SCREEN_SIZE[2])
  gpu:setForeground(1,1,1,1)
  gpu:setBackground(0, 0, 0, 0)
end

function clearScreen()
  gpu:setSize(screenWidth, screenHeight)
  gpu:fill(0, 0, screenWidth, screenHeight, " ")
  gpu:flush()
end

function localTime()
  local hour = string.format("%02d", math.floor((computer.time()/60/60) % 24))
  local minute = string.format("%02d", math.floor((computer.time()/60) % 60))
  return hour .. ":" .. minute
end

function daysSinceLanding()
  return math.floor(computer.time()/60/60/24)
end

function updateScreen()
  local daysSinceLanding = daysSinceLanding()
  local localTime = localTime()

  gpu:setText(1, 1, "Day")
  gpu:setText(11 - #tostring(daysSinceLanding), 1, daysSinceLanding)
  gpu:setText(1, 2, "Time")
  gpu:setText(11 - #tostring(localTime), 2, localTime)
  gpu:flush()
end

clearScreen()
initScreen()

while true do
  event.pull(1)
  updateScreen()
end
